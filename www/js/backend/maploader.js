/**
 * Created by Robert on 10/02/2015.
 */

function canvasDraw() {
    mapCanvas = document.createElement('div');
    mapCanvas.style.height = mapHeight + 'px';
    mapCanvas.style.width = '100%';
    mapCanvas.style.marginTop = '48px';
    mapCanvas.id = 'map-canvas';
    document.getElementById(mapScreen).appendChild(mapCanvas);

    mapDraw();
}

function loadMapScript() {
    if (!mapCanvas) {
        if (navigator.connection.type != Connection.NONE) {
            var mapScript = document.createElement('script');
            mapScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBOrcLZ3YzvVPPy7IqSHXRHDYjjFAdNrcM&callback=canvasDraw';
            document.body.appendChild(mapScript);
        }
    }
}

