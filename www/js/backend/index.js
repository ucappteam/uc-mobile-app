var app = {
    //Part of Phonegap/Cordova setup.
    initialize: function() {
        this.bindEvents();
    },

    bindEvents: function() {
        //Ensure device is ready before running most code.
        document.addEventListener('deviceready', this.onDeviceReady, false);
        //Allow back button functionality. (Android)
        document.addEventListener('backbutton', this.backButton, false)
    },

    onDeviceReady: function() {
        //Build DB (if necessary) + Build menus from DB.
        initializeDB();
        //Construct map if not running on WP.
        if(android || iOS) {
            loadMapScript();
        } else {
            mapFrame = document.createElement('iframe');
            mapFrame.setAttribute('src', 'ms-appx-web://com.uc.ucfinder/www/wpmap.html');
            mapFrame.style.height = '100%';
            mapFrame.style.width = '100%';
            mapFrame.id = 'map-frame';
            mapFrame.name = 'map-frame';
            document.getElementById(mapScreen).appendChild(mapFrame);
            mapFrameContent = mapFrame.contentDocument || mapFrame.contentWindow.document;
            mapCanvas = mapFrameContent.getElementById('map-canvas');
        }
        //Highlight animation adds event listener to objects with highlight-on-click class so they trigger highlight animation
        //TODO: does this need to be here? It's also in listScreen and mainScreen
        highlightAnimation();
    },

    onOnline: function() {
        //Construct map.
        loadMapScript();
    },

    onResume: function() {
        //Construct map.
        loadMapScript();
    },

    //Handle back button presses. (Android)
    backButton: function() {
        searchClose();
        var overlay = document.getElementById('search-overlay');
        classie.remove(overlay, 'overlay-open');

        if (document.getElementById('directions-choice-wrapper').style.display == 'block') {
            document.getElementById('directions-choice-wrapper').style.display = 'none';
        } else {
            //Clear and close search box.
            inputEl.value = '';

            //Scroll screen to top when returning to the landing screen.
            if (previousScreens[previousScreens.length - 1] == 'main-screen') {
                document.getElementById('uc-logo').setAttribute('src', 'img/university-of-canterbury-coat-of-arms-mobile.png');
                document.body.scrollTop = 0;
            }

            //If not on landing screen.
            if (currentScreen != mainScreen) {
                //Animate page transition + update page history.
                var lastScreen = previousScreens.pop();

                if (lastScreen == currentScreen) {
                    lastScreen = previousScreens.pop();
                }

                slideOutRight(currentScreen);
                slideInLeft(lastScreen);
                currentScreen = lastScreen;

                //TODO: Fix jump?
                if (lastScreen != mapScreen && lastScreen != infoScreen && lastScreen != mainScreen) {
                    setTimeout(function () {
                        document.body.scrollTop = previousScroll
                    }, 50);
                }
            } else {
                //Stop GPS
                if (posTracker) {
                    navigator.geolocation.clearWatch(posTracker);
                }
                //Close app if on landing screen.
                navigator.app.exitApp();
            }
        }
    },

    //Handle back button presses. (iOS)
    iOSBackButton: function() {
        searchClose();
        var overlay = document.getElementById('search-overlay');
        classie.remove(overlay, 'overlay-open');

        //Clear and close search box.
        inputEl.value = '';

        //Scroll screen to top and change back arrow to UC logo when returning to the landing screen.
        if (previousScreens[previousScreens.length - 1] == 'main-screen') {
            document.getElementById('back-icon').style.display = 'none';
            document.getElementById('uc-logo').style.display = 'block';
            document.body.scrollTop = 0;
        }

        //If not on landing screen.
        if (currentScreen != mainScreen) {
            //Animate page transition + update page history.
            var lastScreen = previousScreens.pop();

            if (lastScreen == currentScreen) {
                lastScreen = previousScreens.pop();
            }

            slideOutRight(currentScreen);
            slideInLeft(lastScreen);
            currentScreen = lastScreen;

            if (lastScreen != mapScreen || lastScreen != infoScreen || lastScreen != mainScreen) {
                document.body.scrollTop = previousScroll;
            }
        }
    }
};

//Can be used to load js or css file into the header with javascript - currently used to load the iOS specific css if var iOS = true, in index.html
function loadjscssfile(filename, filetype) {
    var fileref;
    if (filetype == "js") { //if filename is a external JavaScript file
        fileref = document.createElement('script');
        fileref.setAttribute("type", "text/javascript");
        fileref.setAttribute("src", filename)
    }
    else if (filetype == "css") { //if filename is an external CSS file
        fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(fileref)
    }
}
