/**
 * Created by rjo72 on 12/10/2014.
 */

var appVersion = '1.5.0';

//Determine whether or not app is running on an iOS or Android device (used for device specific styling).
var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
var android = (navigator.userAgent.match(/(Android)/g) ? true : false);

//The names of POI categories used to generate menu items that group POIs. These menu items will be created in order
// from the first item to the last. Two types of category are represented here. Some correspond to menu items on the
// landing screen. Others correspond to items in menus linked to from the landing screen. In cases where items in this
// array correspond to sub-items in a menu then the location of the final sub-item determines the location of the menu
// on the landing screen.
var categoryNames = [
    'Assembly areas',
    'Defibrillators',
    'Evacuation chairs',
    'Help & info towers',
    'Lecture theatres',
    'Food & drink',
    'Vending machines',
    'Accessible parking',
    'Contractor parking',
    'Coupon purchases',
    'Pay and display',
    'Staff link parking',
    'Staff permit parking',
    'Student permit parking',
	'Taxi pick up points',
    'Bike racks',
    'Bus stops',
    'Libraries',
    'Computer workrooms',
    'Departments',
    'Research centres',
    'ATMs',
    'Baby changing',
    'Printing & copying',
    'Showers',
    'UCSA',
    'Health centre',
    'IT service desks',
    'Security',
    'Study and social spaces',
    'Accessible toilets',
    'Ramps',
    'Lifts',
    'Halls of residence',
	'Follow-You printers',
	'Parking'
];

//POIModel.id values used to pull specific POIs not covered by the categories specified in categoryNames
// (Hopefully temporary).
var individualIds = [
    '10904',
    '11296',
    '10705',
    '10868',
    '10905',
    '10939',
    '10999'
];

//Maps category names to POIModel.category values.
var nameToId = {
    'Departments': 10892,
	'Halls of residence': 10894,
    'Research centres': 10896,
    'Pharmacy & bookshop': 10897,
    'Lecture theatres': 10899,
    'ATMs': 10902,
    'Baby changing': 10903,
    'Post services': 10908,
    'Showers': 10910,
    'Bike racks': 10911,
    'Bus stops': 10912,
    'Assembly areas': 10920,
    'Defibrillators': 10921,
    'Evacuation chairs': 10922,
    'Help & info towers': 10923,
    'Computer workrooms': 10926,
    'Libraries': 10927,
    'Food & drink': 10928,
    'IT service desks': 10932,
    'Printing & copying': 10935,
    'Recreation centre': 10936,
    'Student services': 10939,
    'UCSA': 10940,
    'Study and social spaces': 11036,
    'Health centre': 11049,
    'Vending machines': 11140,
    'Accessible parking': 11271,
    'Contractor parking': 11273,
    'Coupon purchases': 11274,
    'Pay and display':  11275,
    'Staff link parking': 11276,
    'Staff permit parking': 11277,
    'Student permit parking': 11278,
    'Security': 11286,
    'Accessible toilets': 11301,
    'Ramps': 11306,
    'Lifts': 11308,
    'Parking': 11318,
	'Taxi pick up points': 11589,
	'Follow-You printers': 15397
};

//Maps POIModel.category values to category names.
var idToName = {
    10892: 'Departments',
	10894: 'Halls of residence',
    10896: 'Research centres',
    10897: 'Pharmacy & bookshop',
    10899: 'Lecture theatres',
    10902: 'ATMs',
    10903: 'Baby changing',
    10908: 'Post services',
    10910: 'Showers',
    10911: 'Bike racks',
    10912: 'Bus stops',
    10920: 'Assembly areas',
    10921: 'Defibrillators',
    10922: 'Evacuation chairs',
    10923: 'Help & info towers',
    10926: 'Computer workrooms',
    10927: 'Libraries',
    10928: 'Food & drink',
    10932: 'IT service desks',
    10935: 'Printing & copying',
    10936: 'Recreation centre',
    10939: 'Student services',
    10940: 'UCSA',
    11036: 'Study and social spaces',
    11049: 'Health centre',
    11140: 'Vending machines',
    11271: 'Accessible parking',
    11273: 'Contractor parking',
    11274: 'Coupon purchases',
    11275: 'Pay and display',
    11276: 'Staff link parking',
    11277: 'Staff permit parking',
    11278: 'Student permit parking',
    11286: 'Security',
    11301: 'Accessible toilets',
    11306: 'Ramps',
    11308: 'Lifts',
    11318: 'Parking',
	11589: 'Taxi pick up points',
	15397: 'Follow-You printers'	
};

//Maps POIModel.category values to locally stored map marker icon locations.
var categoryImages = {
    10889: 'img/services.png',
	10894: 'img/halls.png',
    10892: 'img/department.png',
    10896: 'img/department.png',
    10897: 'img/services.png',
    10899: 'img/lecture.png',
    10902: 'img/services.png',
    10903: 'img/services.png',
    10908: 'img/services.png',
    10910: 'img/services.png',
    10911: 'img/bicycle.png',
    10912: 'img/bus.png',
    10920: 'img/emergency.png',
    10921: 'img/emergency.png',
    10922: 'img/emergency.png',
    10923: 'img/help-tower.png',
    10926: 'img/computer-workroom.png',
    10927: 'img/library.png',
    10928: 'img/food.png',
    10932: 'img/services.png',
    10935: 'img/services.png',
    10936: 'img/services.png',
    10939: 'img/services.png',
    10940: 'img/services.png',
    11036: 'img/spaces.png',
    11049: 'img/services.png',
    11140: 'img/food.png',
    11271: 'img/parking.png',
    11273: 'img/parking.png',
    11274: 'img/parking.png',
    11275: 'img/parking.png',
    11276: 'img/parking.png',
    11277: 'img/parking.png',
    11278: 'img/parking.png',
    11286: 'img/services.png',
    11301: 'img/access.png',
    11306: 'img/access.png',
    11308: 'img/access.png',
    11318: 'img/access.png',
	11589: 'img/parking.png',
	15397: 'img/printer.png'
};

//Maps landing screen menu item innerHTML values to landing screen icons.
var mainPageImages = {
    'Lecture theatres': 'icon-buildings',
    'Food & drink': 'icon-coffee',
    'Parking': 'icon-cab',
    'Buses & bike stands': 'icon-bike',
    'Libraries': 'icon-book',
    'Computer workrooms': 'icon-ion-android-desktop',
    'Departments': 'icon-graduation-cap',
    'Services & facilities': 'icon-services',
    'Assistance': 'icon-attention-alt',
    'Study and social spaces': 'flaticon-computer',
    'Accessibility': 'flaticon-people',
	'Halls of residence': 'flaticon-web' ,
	'Follow-You printers': 'flaticon-printer'
};

//Used to handle POI categories that require different forms of menu structure.
var singlePoiListCategories = [10894, 10899, 10927, 10892, 10926, 15397];
var parkingCategories = [11271, 11273, 11274, 11275, 11276, 11277, 11278, 11589];
var busBikeCategories = [10911, 10912];
var emergencyCategories = [10920, 10921, 10922, 10923];
var servicesCategories = [10902, 10903, 11049, 10932, 10910, 10935, 10940, 11286];
var foodCategories = [10928, 11140];
var spacesCategories = [11036];
var accessCategories = [11301, 11306, 11308, 11318];

//Food & drink establishments (Not vending machines)
var establishments = '10928';

//Used to combine lecture room subcategory values obtained from the CMS provided JSON data into one unified category.
var lectureCategory = '10899';
var lectureSubCategories = ['10942', '10943', '10944'];

//Used to combine department subcategory values obtained from the CMS provided JSON data into one unified category.
var departmentCategory = '10892';
var departmentSubCategories = ['10896', '10914', '10915', '10916', '10918', '10919'];

//Variables used to provide landing screen menu item names for items that do not use the
// "landing screen -> List of items -> map (single POI)" structure.
var parking = 'Parking';
var busBike = 'Buses & bike stands';
var emergency = 'Assistance';
var services = 'Services & facilities';
var food = 'Food & drink';
var spaces = 'Study and social spaces';
var access = 'Accessibility';
var halls = 'Halls of residence';

//Emergency numbers and corresponding menu item names for the "Emergency" page.
var emergencyNumbers = [
    ['Emergency Services - Police, Fire & Ambulance', '111', 'tel:111'],
    ['Campus Security - Hazards & Emergencies', '0800 823 637', 'tel:0800 823 637'],
    ['Campus Security - General Assistance', '03 364 2888', 'tel:+643 364 2888']
];

//Used to ensure Google watermark fits onto map screen.
var headerHeight = 48;
var mapHeight = document.documentElement.clientHeight - headerHeight;

//Categories that require transitions from the landing screen to the map screen.
var mainToMap = [busBike, spaces];

//The last item placed on the landing screen. Once created the splash screen is hidden.
var lastMainItem = halls;

//Used to keep track of screen and screen scroll history to allow transitions using back buttons.
var currentScreen = 'main-screen';
var previousScreens = [];
var previousScroll = 0;

//Id values for non-list screens
var mainScreen = 'main-screen';
var resultsScreen = 'results-screen';
var mapScreen = 'map-screen';
var infoScreen = 'info-screen';

//Map canvas variable to enable easier reference on WP.
var mapCanvas = null;

//Default map zoom level for when multiple markers are displayed.
var poiGroupZoom = 16;

//Used to determine (roughly) if the user is on campus or not. If within uniPointRadius metres of a uniPoint they
// are treated as being on campus when groups of POIs are displayed on the map.
var uniPointRadius = 225;
var uniPoints = [
    {near: 'James Hight Bldg', lat: -43.523645, lng: 172.583021},
    {near: 'College of Education', lat: -43.521778, lng: 172.567647},
    {near: 'Biology Carpark', lat: -43.522384, lng: 172.584470},
    {near: 'Engineering Road', lat: -43.521688, lng: 172.581552},
    {near: 'Fine Arts', lat: -43.524817, lng: 172.585790},
    {near: 'Law Carpark', lat: -43.525941, lng: 172.583773},
    {near: 'Okeover House', lat: -43.523199, lng: 172.580908},
    {near: 'Te Ao Marama', lat: -43.523141, lng: 172.586680},
    {near: 'Rec Centre', lat: -43.526568, lng: 172.584727},
    {near: 'Sonoda', lat: -43.521832, lng: 172.569771}
];

// Variable for whether search bar is open
var searchBoxOpen = false;
