/**
 * Created by charles on 14/01/15.
 */
//Touch event animations
function scaleAnimation() {
        var elements = document.getElementsByClassName("scale-on-click");

        for (var i = 0; i < elements.length; i++) {
            var elm = elements[i];
            elm.addEventListener("touchstart", function () {
                classie.add(this, 'scale-function');
            }, false);
            elm.addEventListener("touchmove", function () {
                classie.remove(this, "scale-function");
            }, false);
            elm.addEventListener("touchend", function () {
                classie.remove(this, "scale-function");
            }, false);
            elm.addEventListener("touchcancel", function () {
                classie.remove(this, "scale-function");
            }, false);
        }
}

function highlightAnimation() {
        var elements = document.getElementsByClassName("highlight-on-click");

        for (var i = 0; i < elements.length; i++) {
            var elm = elements[i];
            elm.addEventListener("touchstart", function () {
                if (!searchBoxOpen) {
                    classie.add(this, 'highlight-function');
                }
            }, false);
            elm.addEventListener("touchmove", function () {
                classie.remove(this, "highlight-function");
            }, false);
            elm.addEventListener("touchend", function () {
                classie.remove(this, "highlight-function");
            }, false);
            elm.addEventListener("touchcancel", function () {
                classie.remove(this, "highlight-function");
            }, false);
        }
}

//Page Transitions
function slideOutLeft(screen){
    var slidingOutLeft = document.getElementById(screen);
    classie.add(slidingOutLeft, "left");
    classie.remove(slidingOutLeft, "center");
}

function slideOutRight(screen){
    var slidingOutRight = document.getElementById(screen);
    classie.add(slidingOutRight, "right");
    classie.remove(slidingOutRight, "center");
}

function slideInLeft(screen){
    var slidingInLeft = document.getElementById(screen);
    classie.add(slidingInLeft, "center");
    classie.remove(slidingInLeft, "left");
}

function slideInRight(screen){
    var slidingInRight = document.getElementById(screen);
    classie.add(slidingInRight, "center");
    classie.remove(slidingInRight, "right");
}
