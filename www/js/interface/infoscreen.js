/**
 * Created by Robert on 16/12/2014.
 */

//Inserts POI description data into the info screen.
function infoDraw(poi) {
    var infoImage = document.getElementById('info-image');
    var desc = poi[0].description;
    var subtitle = poi[0].subtitle;
    var img = poi[0].image;

    var connection = navigator.connection.type != Connection.NONE;

    if (img && connection) {
        infoImage.style.backgroundImage = 'url("' + 'http://www.canterbury.ac.nz' + img + '")';
    } else if (!connection) {
        infoImage.style.backgroundImage = 'url("./img/no-image.jpg")';
    } else {
        infoImage.style.backgroundImage = 'url("./img/no-image.jpg")';
    }

    var infoSubtitle = document.getElementById('info-subtitle');
    infoSubtitle.innerHTML = subtitle;

    if(subtitle) {
        infoSubtitle.innerHTML += '<hr>';
    }

    infoSubtitle.className = 'info-page-text';

    var infoText = document.getElementById('info-text');
    infoText.innerHTML = desc;
    infoText.className = 'info-page-text';

    var infoTitle = document.getElementById('info-title');
    infoTitle.innerHTML = poi[0].title;

    var infoMapButton = document.getElementById('info-button');
    infoMapButton.addEventListener('click', function() {
        infoMapTransition([poi[0]])
    }, false);

    var links = document.getElementById(infoScreen).getElementsByTagName('a');
    var len = links.length;

    for (var i = 0; i < len; i++) {
        links[i].addEventListener('click', function(e) {
            e.preventDefault();
            var browserWindow = window.open(this.href, '_system', 'location=yes');
            browserWindow.addEventListener("exit", function() {
                browserWindow.close();
            });
        });
    }
}

function infoMapTransition(poi) {
    if (!searchBoxOpen) {
        removeFromArray(mapScreen, previousScreens);

        markerDraw(poi);
        userLocation();

        followUser = false;
        var poiLocation = new google.maps.LatLng(poi[0].lat, poi[0].lng);

        if (userPosition !== null) {
            setBounds(poiLocation);
        } else {
            centerMap();
        }

        previousScreens.push(currentScreen);
        slideOutLeft(currentScreen);
        slideInRight(mapScreen);
        currentScreen = mapScreen;

        overrideLinks();
        google.maps.event.trigger(map, 'resize');
    }
}
