/**
 * Created by Robert on 19/12/2014.
 */

var inputEl = document.getElementById('search');

//Header map icon sends user to map when it is pressed.
document.getElementById('map-icon').addEventListener('click', function() {
    if (currentScreen == mapScreen) {
        app.backButton();
        if(iOS) {
            app.iOSBackButton();
        }
    } else {
        headerMapTransition();
    }
}, false);

//back button in top left corner sends user back one screen when pressed. (iOS only)
document.getElementById('back-icon').addEventListener('click', function() {
    app.iOSBackButton();
}, false);

document.getElementById('uc-logo').addEventListener('click', function() {
    if (currentScreen != mainScreen) {
        slideOutRight(currentScreen);
        slideInLeft(mainScreen);
        currentScreen = mainScreen;
        document.getElementById('uc-logo').setAttribute('src', 'img/university-of-canterbury-coat-of-arms-mobile.png');
        document.body.scrollTop = 0;
    }
}, false);

//Add touch listeners to overlay placed over other UI elements closes the search bar.
var overlay = document.getElementById('search-overlay');
overlay.addEventListener('touchstart', touchListener, false);
overlay.addEventListener('touchmove', touchListener, false);

//Shift overlay to account for iOS status bar.
if (iOS) {
    overlay.style.top = '64px';
}

//Transition from current screen to map with no markers attached.
function headerMapTransition() {
    //Pass empty array to markerDraw() to clear all map markers.
    markerDraw([]);
    //Ensure geolocation is active
    userLocation();
    //Center map on user location if possible.
    map.setZoom(poiGroupZoom);
    centerMap();

    //No transition if already on map screen.
    if (currentScreen != mapScreen) {
        //Animate page transition + update page history.
        previousScroll = document.body.scrollTop;
        previousScreens.push(currentScreen);
        slideOutLeft(currentScreen);
        slideInRight(mapScreen);
        currentScreen = mapScreen;

        //Resize map to ensure map tiles load + force links into inAppBrowser.
        google.maps.event.trigger(map, 'resize');
        overrideLinks();
    }

    //Change top left image to back button. (iOS only.)
    if(iOS) {
        document.getElementById('back-icon').style.display = 'block';
        document.getElementById('uc-logo').style.display = 'none';
    } else {
        document.getElementById('uc-logo').setAttribute('src', 'img/university-of-canterbury-coat-of-arms-mobile-back.png');
    }
}

function searchClick() {
    previousScroll = document.body.scrollTop;

    var searchBoxContents = document.getElementById("search").value;

    if (!searchBoxOpen) {
        searchOpen();
    } else if (searchBoxContents) {
        searchForm();
    } else {
        searchClose();
    }
}

function searchOpen() {
    if(iOS && (currentScreen != mainScreen)) {
        var backIcon = document.getElementById('back-icon');
        classie.add(backIcon, 'slide-icon-out');
    } else {
        var ucLogo = document.getElementById('uc-logo');
        classie.add(ucLogo, 'slide-icon-out');
    }

    var overlay = document.getElementById('search-overlay');
    classie.add(overlay, 'overlay-open');

    var searchBar = document.getElementById('sb-search');
    classie.add(searchBar, 'sb-search-open');

    var searchBox = document.getElementById('search');
    searchBox.focus();

    searchBoxOpen = true;
}

function searchClose() {
    if(iOS && (currentScreen != mainScreen)) {
        var backIcon = document.getElementById('back-icon');
        classie.remove(backIcon, 'slide-icon-out');
    } else {
        var ucLogo = document.getElementById('uc-logo');
        classie.remove(ucLogo, 'slide-icon-out');
    }

    var overlay = document.getElementById('search-overlay');
    classie.remove(overlay, 'overlay-open');

    inputEl.blur();
    var searchBar = document.getElementById('sb-search');
    classie.remove(searchBar, 'sb-search-open');

    searchBoxOpen = false;
}

function touchListener() {
    var overlay = document.getElementById('search-overlay');
    classie.remove(overlay, 'overlay-open');
    setTimeout(function() {
        searchClose();
    }, 300);
}

//Fired on submitting search form. Gets value from field, passes it to the search function in db.js which runs the query,
// deletes any old search results page, slides in the results page.
function searchForm() {

    var searchField = document.getElementById("search").value;

    if (searchField) {
        var myNode = document.getElementById(resultsScreen);
            while (myNode.firstChild) {
             myNode.removeChild(myNode.firstChild);
            }

        searchPOI(searchField, searchSuccess);
        searchClose();

        if (currentScreen != resultsScreen) {
            removeFromArray(resultsScreen, previousScreens);

            previousScreens.push(currentScreen);
            slideOutLeft(currentScreen);
            slideInRight(resultsScreen);
            currentScreen = resultsScreen;

            document.getElementById('uc-logo').style.zIndex = 11;

            if (iOS) {
                document.getElementById('uc-logo').style.display = 'none';
                document.getElementById('back-icon').style.display = 'block';
            }  else {
                document.getElementById('uc-logo').setAttribute('src', 'img/university-of-canterbury-coat-of-arms-mobile-back.png');
            }
        }
    } else {
        searchClose();
    }
}

//Fire search on pressing enter
var enterKey = document.getElementById("search");
enterKey.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) {
        searchForm();
    }
});
